

@extends('layouts.app')
@section('content')

<h1>Cars for Sale</h1>



<table class="table">
<tr>
    <th>Brand</th>
    <th>Price </th>
    <th>Contact</th>
    <th>Status</th>
    @can('seller') 
    <th>Edit</th>
    <th>Delete</th>

    @endcan
</tr>

@foreach($cars as $car)
<tr>
<td> {{$car->brand}}</td> 
<td> {{$car->price}} </td>
<td> {{$car->contact}}</td> 
<td>
    @if ($car->status==1)
        <h4>SOLD!</h4>
    @else
        @can('seller')
       <a href="{{route('sold', $car->id)}}">Mark as Sold</a>
        @endcan
        @can('buyer')
        <a href="{{route('buy', $car->id)}}">Buy</a>
        @endcan
    @endif
</td>
<td>@can('seller')<a href = "{{route('cars.edit',$car->id)}}">  Edit</a>@endcan</td>
<td>
@can('seller')<a href="{{route('delete', $car->id)}}">Delete</a>@endcan
</td>
</tr>

@endforeach
</table>
<br><br>
@can('seller')
<h3><a href = "{{route('cars.create')}}">Add a new car </a></h3>

@endcan

<!-- @if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
@endif -->

<script>
  var msg = '{{Session::get('alert')}}';
  var exist = '{{Session::has('alert')}}';
  if(exist){
    alert(msg);
  }
</script>
   @endsection
