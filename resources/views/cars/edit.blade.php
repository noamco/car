@extends('layouts.app')
@section('content')
<h1>Edit Car</h1>
<form method = 'POST' action = "{{action('CarController@update' , $car->id)}}">
@method('PATCH')
@csrf
<div class = "form-group">
<label for = "price">Brand</label>
<input type = "text" class= "form-control" name = "brand" value = "{{$car->brand}}">
<label for = "price">Price</label>
<input type = "text" class= "form-control" name = "price" value = "{{$car->price}}">
<label for = "price">Contact Info</label>
<input type = "text" class= "form-control" name = "contact" value = "{{$car->contact}}">

</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>
</form>

<!-- add following if asked for verification -->
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



@endsection