<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert(
            [    
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1,
                'brand' => 'Subaru',
                'price'=> 10000,
                'status'=>0,
                'contact'=>'111'
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1,
                'brand' => 'Toyota',
                'price'=> 20000,
                'status'=>0,
                'contact'=>'111',
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 3,
                'brand' => 'KIA',
                'price'=> 20000,
                'status'=>0,
                'contact'=>'333',
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 3,
                'brand' => 'MG',
                'price'=> 10000,
                'status'=>0,
                'contact'=>'333',
                ],
                
            ]
            );
    }
}
