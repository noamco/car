<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;//verification
class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            $id = Auth::id();
            $user = User::Find($id);
            $cars = Car::All();
            return view('cars.index', ['cars' => $cars]);
        }

            $id = Auth::id();
            $user = User::Find($id);
            $cars = $user->cars;;
         //   return view('cars.index', ['cars' => $cars]);  
            return view('cars.index', compact('cars'));
    }
    return redirect()->intended('/home');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            return view ('cars.create');
        }
        return redirect()->intended('/home');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            //add following if asked for verification
            
                    $this->validate($request,[
                        'brand'=>'required',
                        'price'=>'required',
                        'contact'=>'required',
                        ]
                    );
            //
                            
                    $car = new Car();
                    $id = Auth::id(); //the id of the current user
                    $car->brand = $request->brand;
                    $car->price = $request->price;
                    $car->contact = $request->contact;
                    $car->user_id = $id;
                    $car->status = 0;
                    $car->save();
                    return redirect('cars');  
                }
                return redirect()->intended('/home');
                 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            $car = Car::find($id);
        return view('cars.edit', compact('car'));
            }
        return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       if (Auth::check()) {
        $this->validate($request,[
            'brand'=>'required',
            'price'=>'required',
            'contact'=>'required',            ]
//
        );

       $car = Car::find($id);
       $car->update($request->except(['_token']));
  
    //   if($request->ajax()){
    //     if (Gate::denies('seller')) {
    //         abort(403,"You are not allowed to edit cars");
    //    }
    //        return Response::json(array('result' => 'success1','status' => $request->status ), 200);
    //    } else {          
           return redirect('cars');           
       }
    return redirect()->intended('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"You are not allowed to delete cars");
       }
        $car = Car::find($id);
        $car->delete();
        return redirect('cars')->with('alert', 'Car has been deleted');;
        }
    return redirect()->intended('/home');
    }
    public function sold($id)
    {
        if (Auth::check()) {
           
        $car = Car::findOrFail($id);            
        $car->status = 1; 
        $id = Auth::id();
        $user= User::find($id);
        $user->credit=$user->credit + $car->price;
        $car->save();
        $user->save();
        return redirect('cars')->with('alert', 'Congrats on selling your car! Your credit is now ' .$user->credit);    
        }
    return redirect()->intended('/home');
    }
    public function buy($id)
    {
        if (Auth::check()) {
           
        $car = Car::findOrFail($id);            
        $car->status = 1; 
        $id = Auth::id();
        $user= User::find($id);
        $user->credit=$user->credit - $car->price;
        $car->save();
        $user->save();
        return redirect('cars')->with('alert', 'Enjoy your new car! Your credit is now ' .$user->credit);    
        }
    return redirect()->intended('/home');
    }
    // public function buyertoseller($id)
    // {
    //     if (Auth::check()) {
    //     $id = Auth::id();
    //     $user = User::findOrFail($id);            
    //     $user->role = 1; 
    //     $user->save();
    //     return redirect('cars');    
    //     }
    // return redirect()->intended('/home');
    // }
    // public function sellertobuyer($id)
    // {
    //     if (Auth::check()) {
    //     $id = Auth::id();   
    //     $user = User::findOrFail($id);            
    //     $user->role = 0; 
    //     $user->save();
    //     return redirect('cars');    
    //     }
    // return redirect()->intended('/home');
    // }
}
